package ramazani.amirhossein.notetakingapp.ui.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import ramazani.amirhossein.notetakingapp.R;
import ramazani.amirhossein.notetakingapp.custom_view.SketchView;
import ramazani.amirhossein.notetakingapp.model.ModelCategory;
import ramazani.amirhossein.notetakingapp.model.ModelNote;
import ramazani.amirhossein.notetakingapp.utils.Constants;
import ramazani.amirhossein.notetakingapp.utils.data_sources.DataSource;
import ramazani.amirhossein.notetakingapp.utils.helper.FileHelper;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by amirhossein on 12/11/2016.
 */

public class ActivityPaint extends AppCompatActivity implements SketchView.OnDrawChangedListener {

    private static final int INTENT_NOTE_DETAILS = 123;
    private Toolbar mToolbar;

    private AppCompatEditText mPaintNoteEdtTitle;
    private SketchView mDrawingView;

    private ModelNote mCurrentNote;
    private ModelNote mModifiedNote;

    private Uri attachmentUri;

    private DataSource mDataSource;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDataSource = new DataSource(this);
        setContentView(R.layout.activity_paint_note);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mPaintNoteEdtTitle = (AppCompatEditText) findViewById(R.id.paintNoteEdtTitle);

        mDrawingView = (SketchView) findViewById(R.id.drawingView);
        mDrawingView.setOnDrawChangedListener(this);
        initPaintProperties();
        handleIntent();
    }

    private void initPaintProperties() {
        File file = FileHelper.createPaintFile(this);
        attachmentUri = Uri.fromFile(file);
    }

    private void handleIntent() {
        Intent intent = getIntent();
        mModifiedNote = new ModelNote();
        if (intent.hasExtra(Constants.INTENT_CURRENT_NOTE)) {
            mCurrentNote = intent.getExtras().getParcelable(Constants.INTENT_CURRENT_NOTE);
            Log.i(FileHelper.TAG, "handleIntent: current note uri : " + mCurrentNote.getUri().toString());
            mModifiedNote
                .setNoteId(mCurrentNote.getNoteId())
                .setTitle(mCurrentNote.getTitle())
                .setUri(attachmentUri);

            initValues();
        } else {

            if (getIntent().getExtras().getParcelable(Constants.INTENT_CURRENT_CATEGORY) != null) {
                mModifiedNote.setCategory((ModelCategory) getIntent().getExtras().getParcelable(Constants.INTENT_CURRENT_CATEGORY));
            }

            mModifiedNote
                .setUri(attachmentUri);
        }
    }

    private void initValues() {
        mPaintNoteEdtTitle.append(mCurrentNote.getTitle());
        loadBitmap();
    }

    private void loadBitmap() {
        Bitmap bmp = null;
        try {
            bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(mCurrentNote.getUri()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        mDrawingView.setBackgroundBitmap(this, bmp);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_note_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                showAlertDialog();
                return true;
            case R.id.menu_save:
                save();
                return true;
            case R.id.menu_style:
                return true;
            case R.id.menu_categorize:
                Intent intent = new Intent(this, ActivityCategories.class);
                startActivityForResult(intent, INTENT_NOTE_DETAILS);
                return true;
            case R.id.menu_remove_category:
                mModifiedNote.setCategory(null);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == INTENT_NOTE_DETAILS) {
            if (resultCode == RESULT_OK) {
                ModelCategory category = (ModelCategory) data.getExtras().getSerializable(Constants.INTENT_SELECTED_CATEGORY);
                mModifiedNote.setCategory(category);
            }
        }
    }

    private void save() {
        Bitmap bitmap = mDrawingView.getBitmap();
        if (bitmap != null) {
            try {
                File bitmapFile = new File(attachmentUri.getPath());
                FileOutputStream out = new FileOutputStream(bitmapFile);
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
                out.close();
                if (!bitmapFile.exists()) {
                    // TODO : Error ! CANT FIND BITMAP
                }

            } catch (Exception e) {

            }
        } else {
            if (mCurrentNote != null)
                mModifiedNote.setUri(mCurrentNote.getUri());
        }

        if (mCurrentNote != null) {
            File file = new File(mCurrentNote.getUri().getPath());
            file.delete();
        }

        mModifiedNote
            .setIsText(false)
            .setTitle(mPaintNoteEdtTitle.getText().toString());

        if (mCurrentNote == null) {
            mDataSource.createNote(this.mModifiedNote);
        } else {
            //update note
            mDataSource.updateNote(this.mModifiedNote);
        }

        finish();
    }

    private void showAlertDialog() {
        new AlertDialog.Builder(this)
            .setMessage(getString(R.string.do_you_really_discard))
            .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            })
            .setNeutralButton(getString(R.string.save_and_exit), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    save();
                }
            })
            .setPositiveButton(getString(R.string.discard), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    ActivityPaint.super.onBackPressed();
                }
            })
            .show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        showAlertDialog();
    }

    @Override
    public void onDrawChanged() {
/*
        // Undo
        if (mDrawingView.getPaths().size() > 0)
            AlphaManager.setAlpha(undo, 1f);
        else
            AlphaManager.setAlpha(undo, 0.4f);
        // Redo
        if (mDrawingView.getUndoneCount() > 0)
            AlphaManager.setAlpha(redo, 1f);
        else
            mDrawingView.setAlpha(redo, 0.4f);
*/
    }
}
