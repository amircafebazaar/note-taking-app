package ramazani.amirhossein.notetakingapp.ui.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ramazani.amirhossein.notetakingapp.MainActivity;
import ramazani.amirhossein.notetakingapp.R;
import ramazani.amirhossein.notetakingapp.adapter.AdapterNote;
import ramazani.amirhossein.notetakingapp.listener.ClickListener;
import ramazani.amirhossein.notetakingapp.model.ModelCategory;
import ramazani.amirhossein.notetakingapp.model.ModelNote;
import ramazani.amirhossein.notetakingapp.ui.activities.ActivityNoteDetails;
import ramazani.amirhossein.notetakingapp.ui.activities.ActivityPaint;
import ramazani.amirhossein.notetakingapp.utils.Constants;
import ramazani.amirhossein.notetakingapp.utils.data_sources.DataSource;
import ramazani.amirhossein.notetakingapp.utils.helper.HelperAnimation;

/**
 * Created by amirhossein on 12/8/2016.
 */

public class FragmentNotes extends Fragment implements ClickListener, FloatingActionButton.OnClickListener {

    public static final String TAG = FragmentNotes.class.getSimpleName();

    // data
    private DataSource mDataSource;
    private ModelCategory mCurrentCategory;

    private FloatingActionButton mFab;
    private FloatingActionButton mFabText;
    private FloatingActionButton mFabPaint;


    private RecyclerView mRecyclerView;

    private AdapterNote mAdapterNote;
    private List<ModelNote> mNotes;

    private ActionModeCallback actionModeCallback = new ActionModeCallback();
    private ActionMode actionMode;
    private boolean isActionModeEnable;

    private ProgressDialog mProgressDialog;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notes, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mFabText.getAlpha() > 0.0f && mFabPaint.getAlpha() > 0.0f)
            mFab.performClick();
        init();
    }

    private void init() {
        mDataSource = new DataSource(getActivity());
        int catId = getArguments().getInt(Constants.ARGUMENT_CAT_ID);
        if (catId == 0) {
            mCurrentCategory = new ModelCategory();
            mCurrentCategory.setCategoryId(0);
        } else {
            mCurrentCategory = mDataSource.getCategory(catId);
        }

        initView();
    }

    private void initView() {

        mFab = (FloatingActionButton) getView().findViewById(R.id.fabNewNote);
        mFabText = (FloatingActionButton) getView().findViewById(R.id.fabNewTextNote);
        mFabPaint = (FloatingActionButton) getView().findViewById(R.id.fabNewPaintNote);

        mFab.setOnClickListener(this);
        mFabText.setOnClickListener(this);
        mFabPaint.setOnClickListener(this);

        mRecyclerView = (RecyclerView) getView().findViewById(R.id.recycler);

        getNotes();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (!isActionModeEnable)
                    if (dy > 0 || dy < 0 && mFab.isShown())
                        mFab.hide();
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (!isActionModeEnable)
                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        mFab.show();
                    }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage(getString(R.string.deleting___));

    }

    private MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabNewNote:
                if (mFabText.getAlpha() > 0.0f && mFabPaint.getAlpha() > 0.0f) {
                    HelperAnimation.rotateAnimate(mFab, false);
                    HelperAnimation.setAnimate(mFabPaint, false);
                    HelperAnimation.setAnimate(mFabText, false);
                } else {
                    HelperAnimation.rotateAnimate(mFab, true);
                    HelperAnimation.setAnimate(mFabPaint, true);
                    HelperAnimation.setAnimate(mFabText, true);
                }
                break;
            case R.id.fabNewTextNote:
                HelperAnimation.rotateAnimate(mFab, true);
                HelperAnimation.setAnimate(mFabPaint, true);
                HelperAnimation.setAnimate(mFabText, true);
                Intent intent1 = new Intent(getMainActivity(), ActivityNoteDetails.class);
                intent1.putExtra(Constants.INTENT_CURRENT_CATEGORY, mCurrentCategory);
                getMainActivity().startActivity(intent1);
                break;
            case R.id.fabNewPaintNote:
                HelperAnimation.rotateAnimate(mFab, true);
                HelperAnimation.setAnimate(mFabPaint, true);
                HelperAnimation.setAnimate(mFabText, true);
                Intent intent2 = new Intent(getMainActivity(), ActivityPaint.class);
                intent2.putExtra(Constants.INTENT_CURRENT_CATEGORY, mCurrentCategory);
                getMainActivity().startActivity(intent2);
                break;
        }
    }

    @Override
    public void onItemClicked(int position) {
        if (actionMode != null) {
            toggleSelection(position);
        } else {
            Class intentClass = null;
            if (mNotes.get(position).isText()) {
                intentClass = ActivityNoteDetails.class;
            } else {
                intentClass = ActivityPaint.class;
            }

            Intent intent = new Intent(getMainActivity(), intentClass);
            intent.putExtra(Constants.INTENT_CURRENT_NOTE, mNotes.get(position));
            startActivity(intent);
        }
    }

    @Override
    public boolean onItemLongClicked(int position) {
        if (actionMode == null) {
            actionMode = getMainActivity().startSupportActionMode(actionModeCallback);
        }

        toggleSelection(position);

        return true;
    }

    private void toggleSelection(int position) {
        mAdapterNote.toggleSelection(position);
        int count = mAdapterNote.getSelectedItemCount();

        if (count == 0) {
            actionMode.finish();
        } else {
            actionMode.setTitle(String.valueOf(count));
            actionMode.invalidate();
        }
    }

    private void selectAllItems() {
        for (int i = 0; i < mNotes.size(); i++) {
            mAdapterNote.selectItem(i);
        }
    }

    private void deSelectAllItems() {
        for (int i = 0; i < mNotes.size(); i++) {
            mAdapterNote.deSelectItem(i);
        }
    }

    private class ActionModeCallback implements ActionMode.Callback {
        @SuppressWarnings("unused")
        private final String TAG = ActionModeCallback.class.getSimpleName();

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.menu_notes_selected, menu);
            isActionModeEnable = true;
            mFabPaint.hide();
            mFabText.hide();
            mFab.hide();
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_remove:
                    removeNotes(mAdapterNote.getSelectedItems());
                    mAdapterNote.clearSelection();
                    actionMode.finish();
                    return true;
                case R.id.menu_select_all:
                    selectAllItems();
                    mode.setTitle(mNotes.size() + "");
                    return true;

                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            isActionModeEnable = false;
            mAdapterNote.clearSelection();
            mFab.show();
            actionMode = null;
        }
    }

    private void getNotes() {
        AsyncNote asyncNote = new AsyncNote();
        asyncNote.execute();
    }

    private void removeNotes(List<Integer> selectedItems) {
        AsyncRemoveNotes asyncNote = new AsyncRemoveNotes(selectedItems);
        asyncNote.execute();
    }

    private class AsyncNote extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            mNotes = mDataSource.getNotes(mCurrentCategory.getCategoryId());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mAdapterNote = new AdapterNote(getActivity(), mNotes, FragmentNotes.this);
            mRecyclerView.setAdapter(mAdapterNote);
        }
    }

    private class AsyncRemoveNotes extends AsyncTask<Void, Void, Void> {

        private List<Integer> positions;

        public AsyncRemoveNotes(List<Integer> positions) {
            this.positions = positions;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... lists) {
            List<ModelNote> notes = new ArrayList<>();
            for (int i : positions) {
                notes.add(mNotes.get(i));
            }
            mDataSource.removeNotes(notes);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mAdapterNote.removeItems(positions);
            mProgressDialog.dismiss();
            getMainActivity().loadNavigationMenuItems();
        }
    }
}
