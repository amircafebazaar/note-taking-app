package ramazani.amirhossein.notetakingapp.ui.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.ListViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ramazani.amirhossein.notetakingapp.MainActivity;
import ramazani.amirhossein.notetakingapp.R;
import ramazani.amirhossein.notetakingapp.utils.helper.DataHelper;
import ramazani.amirhossein.notetakingapp.utils.helper.DateHelper;

/**
 * Created by amirhossein on 12/16/2016.
 */

public class FragmentSettings extends Fragment implements View.OnClickListener {

    private AppCompatTextView btnBackup;
    private AppCompatTextView btnRestore;

    private AppCompatEditText edtFileName;
    private ListViewCompat lstFiles;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnBackup = (AppCompatTextView) getView().findViewById(R.id.btnBackup);
        btnRestore = (AppCompatTextView) getView().findViewById(R.id.btnRestore);
        btnBackup.setOnClickListener(this);
        btnRestore.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBackup:
                backupData();
                break;
            case R.id.btnRestore:
                restoreData();
        }
    }

    private void backupData() {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_export, null);

        edtFileName = (AppCompatEditText) view.findViewById(R.id.edtFileName);
        edtFileName.setHint(DateHelper.formatNow(DateHelper.TYPE_NAME));

        final AlertDialog alertDialog =
            new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialogInterface, int i) {
                        final String fileName;
                        if (edtFileName.getText().toString().equals(""))
                            fileName = edtFileName.getHint().toString();
                        else
                            fileName = edtFileName.getText().toString();

                        DataHelper.exportDB(getActivity(), fileName);
                        dialogInterface.dismiss();
                    }
                })
                .create();


        alertDialog.show();
    }

    private void restoreData() {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_import, null);

        lstFiles = (ListViewCompat) view.findViewById(R.id.lstFiles);
        final File[] files = DataHelper.getBackupList();

        if (files == null || files.length == 0) {
            Toast.makeText(getActivity(), "No Backup Found", Toast.LENGTH_SHORT).show();
            return;
        }
        List<String> values = new ArrayList<>();
        for (File file : files) {
            values.add(file.getName());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.item_textview, values);
        lstFiles.setAdapter(adapter);

        final AlertDialog alertDialog =
            new AlertDialog.Builder(getActivity())
                .setView(view)
                .create();

        lstFiles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                DataHelper.importDB(getActivity(), files[i]);
                getMainActivity().mDataSource.reInit(getMainActivity());
                alertDialog.dismiss();
            }
        });

        alertDialog.show();

    }

    private MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

}
