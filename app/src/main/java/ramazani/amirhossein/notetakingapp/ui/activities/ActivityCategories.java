package ramazani.amirhossein.notetakingapp.ui.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.color.ColorChooserDialog;

import java.util.List;

import ramazani.amirhossein.notetakingapp.R;
import ramazani.amirhossein.notetakingapp.adapter.AdapterCategory;
import ramazani.amirhossein.notetakingapp.model.ModelCategory;
import ramazani.amirhossein.notetakingapp.utils.Constants;
import ramazani.amirhossein.notetakingapp.utils.data_sources.DataSource;
import ramazani.amirhossein.notetakingapp.utils.helper.ConvertHelper;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by amirhossein on 12/12/2016.
 */

public class ActivityCategories extends AppCompatActivity implements AdapterCategory.CategoryViewHolder.ClickListener, ColorChooserDialog.ColorCallback {

    public static final String TAG = ActivityCategories.class.getSimpleName();

    private AdapterCategory mAdapterCategory;
    private List<ModelCategory> mCategories;
    private RecyclerView mRecyclerView;

    private ActionModeCallback actionModeCallback = new ActionModeCallback();
    private ActionMode actionMode;
    private DataSource mDataSource;

    private FloatingActionButton fabNewCategory;
    private Toolbar mToolbar;

    //Dialog Components
    private Dialog mDialog;
    private AppCompatEditText edtTitle;
    private AppCompatImageView imgColorChooser;
    private AppCompatButton btnOk;
    private AppCompatButton btnDelete;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        mDataSource = new DataSource(this);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fabNewCategory = (FloatingActionButton) findViewById(R.id.fabNewCategory);

        fabNewCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editCategory(null);
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler);
        getCategories();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);

    }

    public void confirmed(int position) {
        Intent intent = new Intent();
        intent.putExtra(Constants.INTENT_SELECTED_CATEGORY, mCategories.get(position));
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onItemClicked(int position) {
        if (actionMode != null) {
            toggleSelection(position);
        } else {
            confirmed(position);
        }
    }

    private void editCategory(final ModelCategory category) {

        final boolean shouldCreateCategory = (category == null);

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_new_category);

        edtTitle = (AppCompatEditText) dialog.findViewById(R.id.edtDialogCategoryTitle);
        imgColorChooser = (AppCompatImageView) dialog.findViewById(R.id.imgDialogColorChooser);
        btnOk = (AppCompatButton) dialog.findViewById(R.id.btnDialogOk);
        btnDelete = (AppCompatButton) dialog.findViewById(R.id.btnDialogDelete);

        if (!shouldCreateCategory) {
            edtTitle.append(category.getName());
            imgColorChooser.getDrawable().mutate().setColorFilter(Color.parseColor(category.getColor()), PorterDuff.Mode.SRC_ATOP);
        } else {
            btnDelete.setVisibility(View.GONE);
        }

        imgColorChooser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ColorChooserDialog.Builder(ActivityCategories.this, R.string.color_choose)
                    .titleSub(R.string.color_choose)
                    .accentMode(true)
                    .doneButton(R.string.md_done_label)
                    .cancelButton(R.string.md_cancel_label)
                    .backButton(R.string.md_back_label)
                    .dynamicButtonColor(true)
                    .show();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (shouldCreateCategory) {

                    String color = ConvertHelper.convertColorToString(Color.GRAY);
                    if (imgColorChooser.getTag() != null)
                        color = imgColorChooser.getTag().toString();
                    ModelCategory newCat = new ModelCategory()
                        .setColor(color)
                        .setName(edtTitle.getText().toString());

                    mDataSource.createCategory(newCat);
                    getCategories();
                    dialog.dismiss();

                } else {
                    String color = category.getColor();
                    if (imgColorChooser.getTag() != null)
                        color = imgColorChooser.getTag().toString();

                    ModelCategory newCat = new ModelCategory()
                        .setCategoryId(category.getCategoryId())
                        .setColor(color)
                        .setName(edtTitle.getText().toString());

                    mDataSource.updateCategory(newCat);
                    getCategories();
                    dialog.dismiss();
                }
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(ActivityCategories.this)
                    .setMessage(getString(R.string.are_you_sure))
                    .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mDataSource.removeCategory(category);
                            Toast.makeText(ActivityCategories.this, getString(R.string.removed_successfully), Toast.LENGTH_SHORT).show();
                            getCategories();
                            dialog.dismiss();
                        }
                    })
                    .show();

            }
        });

        dialog.show();
    }

    @Override
    public void onColorSelection(@NonNull ColorChooserDialog dialog, @ColorInt int selectedColor) {
        imgColorChooser.getDrawable().mutate().setColorFilter(selectedColor, PorterDuff.Mode.SRC_ATOP);
        imgColorChooser.setTag(ConvertHelper.convertColorToString(selectedColor));
        Log.i(TAG, "onColorSelection: " + selectedColor);
        Log.i(TAG, "onColorSelection: " + imgColorChooser.getTag().toString());
    }

    @Override
    public boolean onItemLongClicked(int position) {
        if (actionMode == null) {
            actionMode = startSupportActionMode(actionModeCallback);
        }

        toggleSelection(position);

        return true;
    }

    private void toggleSelection(int position) {
        mAdapterCategory.toggleSelection(position);
        int count = mAdapterCategory.getSelectedItemCount();

        if (count == 0) {
            actionMode.finish();
        } else {
            actionMode.setTitle(String.valueOf(count));
            actionMode.invalidate();
        }
    }

    private void selectAllItems() {
        for (int i = 0; i < mCategories.size(); i++) {
            mAdapterCategory.selectItem(i);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private class ActionModeCallback implements ActionMode.Callback {
        @SuppressWarnings("unused")
        private final String TAG = ActionModeCallback.class.getSimpleName();

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            fabNewCategory.show();
            mode.getMenuInflater().inflate(R.menu.menu_category_selcted, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            if (mAdapterCategory.getSelectedItemCount() == 1)
                menu.findItem(R.id.menu_edit).setVisible(true);
            else
                menu.findItem(R.id.menu_edit).setVisible(false);

            return true;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_remove:
                    mAdapterCategory.removeItems(mAdapterCategory.getSelectedItems());
                    actionMode.finish();
                    return true;
                case R.id.menu_select_all:
                    selectAllItems();
                    mode.setTitle(mCategories.size() + "");
                    return true;
                case R.id.menu_edit:
                    editCategory(mCategories.get(mAdapterCategory.getSelectedItems().get(0)));
                    actionMode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mAdapterCategory.clearSelection();
            fabNewCategory.show();
            actionMode = null;
        }
    }

    private void refreshRecycler() {
        mAdapterCategory = new AdapterCategory(ActivityCategories.this, mCategories, ActivityCategories.this);
        mRecyclerView.setAdapter(mAdapterCategory);
    }

    private void getCategories() {
        AsyncGetCategories getCategories = new AsyncGetCategories();
        getCategories.execute();
    }

    private class AsyncGetCategories extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            mCategories = mDataSource.getCategories();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            refreshRecycler();
        }
    }

}
