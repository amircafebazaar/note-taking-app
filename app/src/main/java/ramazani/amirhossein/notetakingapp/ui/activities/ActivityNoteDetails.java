package ramazani.amirhossein.notetakingapp.ui.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;

import java.util.Calendar;

import ramazani.amirhossein.notetakingapp.R;
import ramazani.amirhossein.notetakingapp.model.ModelCategory;
import ramazani.amirhossein.notetakingapp.model.ModelNote;
import ramazani.amirhossein.notetakingapp.utils.Constants;
import ramazani.amirhossein.notetakingapp.utils.data_sources.DataSource;
import ramazani.amirhossein.notetakingapp.utils.helper.DateHelper;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by amirhossein on 12/11/2016.
 */

public class ActivityNoteDetails extends AppCompatActivity {

    private static final int INTENT_NOTE_DETAILS = 120;

    private Toolbar mToolbar;
    private AppCompatEditText edtTitle;
    private AppCompatEditText edtContent;
    private AppCompatTextView txtLastModification;
    private AppCompatImageView imgColor;

    private ModelNote mCurrentNote;
    private ModelNote mModifiedNote;

    private DataSource mDataSource;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDataSource = new DataSource(this);

        setContentView(R.layout.activity_note_details);
        initViews();

        handleIntent();
    }

    private void handleIntent() {
        mModifiedNote = new ModelNote();
        if (getIntent().hasExtra(Constants.INTENT_CURRENT_NOTE)) {
            mCurrentNote = getIntent().getExtras().getParcelable(Constants.INTENT_CURRENT_NOTE);
            initializeValues();
            mModifiedNote.setCategory(mCurrentNote.getCategory())
                .setNoteId(mCurrentNote.getNoteId())
                .setIsText(true);
        } else {

            if (getIntent().getExtras().getParcelable(Constants.INTENT_CURRENT_CATEGORY) != null) {
                mModifiedNote.setCategory((ModelCategory) getIntent().getExtras().getParcelable(Constants.INTENT_CURRENT_CATEGORY));
            }

            mModifiedNote
                .setIsText(true)
                .setStyle(Typeface.NORMAL);
        }

    }

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        edtTitle = (AppCompatEditText) findViewById(R.id.noteDetailEdtTitle);
        edtContent = (AppCompatEditText) findViewById(R.id.noteDetailEdtContent);
        txtLastModification = (AppCompatTextView) findViewById(R.id.noteDetailUpdateTime);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void initializeValues() {
        edtTitle.setText(mCurrentNote.getTitle());
        edtContent.setText(mCurrentNote.getContent());
        if (mCurrentNote.getStyle() != -1) {
            edtContent.setTypeface(edtContent.getTypeface(), mCurrentNote.getStyle());
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mCurrentNote.getLastModification());
        txtLastModification.setText(getString(R.string.note_last_modified) + " : " + DateHelper.formatCalendar(calendar));
    }

    private void showPopup(View anchor) {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_text_style, null);

        final PopupWindow popupWindow = new PopupWindow(this);
        popupWindow.setContentView(view);
        popupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.normalTextStyle:
                        mModifiedNote.setStyle(Typeface.NORMAL);
                        edtContent.setTypeface(Typeface.createFromAsset(getAssets(), CalligraphyConfig.get().getFontPath()), Typeface.NORMAL);
                        popupWindow.dismiss();
                        break;
                    case R.id.boldTextStyle:
                        mModifiedNote.setStyle(Typeface.BOLD);
                        edtContent.setTypeface(edtContent.getTypeface(), Typeface.BOLD);
                        popupWindow.dismiss();
                        break;
                    case R.id.italicTextStyle:
                        mModifiedNote.setStyle(Typeface.ITALIC);
                        edtContent.setTypeface(edtContent.getTypeface(), Typeface.ITALIC);
                        popupWindow.dismiss();
                        break;
                }
            }
        };

        LinearLayoutCompat normalText = (LinearLayoutCompat) view.findViewById(R.id.normalTextStyle);
        normalText.setOnClickListener(listener);
        LinearLayoutCompat boldText = (LinearLayoutCompat) view.findViewById(R.id.boldTextStyle);
        boldText.setOnClickListener(listener);
        LinearLayoutCompat italicText = (LinearLayoutCompat) view.findViewById(R.id.italicTextStyle);
        italicText.setOnClickListener(listener);

        popupWindow.showAsDropDown(anchor);
    }

    private void showAlertDialog() {
        new AlertDialog.Builder(this)
            .setMessage(getString(R.string.do_you_really_discard))
            .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            })
            .setNeutralButton(getString(R.string.save_and_exit), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    save();
                }
            })
            .setPositiveButton(getString(R.string.discard), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    ActivityNoteDetails.super.onBackPressed();
                }
            })
            .show();
    }

    private void save() {
        mModifiedNote.setTitle(edtTitle.getText().toString());
        mModifiedNote.setContent(edtContent.getText().toString());

        if (mCurrentNote == null) {
            //create note
            mDataSource.createNote(this.mModifiedNote);
        } else {
            //update note
            mDataSource.updateNote(this.mModifiedNote);
        }

        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_note_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                showAlertDialog();
                return true;
            case R.id.menu_save:
                save();
                return true;
            case R.id.menu_style:
                showPopup(findViewById(R.id.menu_style));
                return true;
            case R.id.menu_categorize:
                Intent intent = new Intent(this, ActivityCategories.class);
                startActivityForResult(intent, INTENT_NOTE_DETAILS);
                return true;
            case R.id.menu_remove_category:
                mModifiedNote.setCategory(null);
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == INTENT_NOTE_DETAILS) {
            if (resultCode == RESULT_OK) {
                ModelCategory category = (ModelCategory) data.getExtras().getParcelable(Constants.INTENT_SELECTED_CATEGORY);
                mModifiedNote.setCategory(category);
            }
        }
    }

    @Override
    public void onBackPressed() {
        showAlertDialog();
    }

}
