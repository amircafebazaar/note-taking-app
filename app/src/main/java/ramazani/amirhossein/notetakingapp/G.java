package ramazani.amirhossein.notetakingapp;

import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by amirhossein on 12/9/2016.
 */

public class G extends Application {

    public static final String TAG = Application.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        if (getString(R.string.app_name).equals(getString(R.string.app_name_check)))
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/english.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
            );
        else
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/persian.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
            );
    }
}
