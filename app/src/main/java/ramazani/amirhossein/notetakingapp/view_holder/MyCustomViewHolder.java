package ramazani.amirhossein.notetakingapp.view_holder;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import ramazani.amirhossein.notetakingapp.R;
import ramazani.amirhossein.notetakingapp.listener.ClickListener;
import ramazani.amirhossein.notetakingapp.model.ModelNote;

/**
 * Created by amirhossein on 12/15/2016.
 */

public abstract class MyCustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    public CardView cardView;
    public ClickListener mListener;
    public Context mContext;

    public MyCustomViewHolder(View itemView, ClickListener listener, Context context) {
        super(itemView);
        this.cardView = (CardView) itemView.findViewById(R.id.cardNote);
        this.mListener = listener;
        this.mContext = context;
        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (mListener != null) {
            mListener.onItemClicked(getLayoutPosition());
        }
    }

    @Override
    public boolean onLongClick(View view) {
        if (mListener != null) {
            return mListener.onItemLongClicked(getLayoutPosition());
        }

        return false;
    }

    public abstract void update(ModelNote note);

}
