package ramazani.amirhossein.notetakingapp.utils.helper;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;

/**
 * Created by amirhossein on 12/15/2016.
 */

public class HelperAnimation {

    public static final void setAnimate(View view, boolean setVisible) {
        float startAlpha = setVisible ? 0.0f : 1.0f;
        float endAlpha = setVisible ? 1.0f : 0.0f;

        view.setPivotX(0);
        view.setPivotY(0);

        ObjectAnimator alpha = ObjectAnimator.ofFloat(view, "alpha", startAlpha, endAlpha);
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(view, "scaleX", startAlpha, endAlpha);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(view, "scaleY", startAlpha, endAlpha);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(alpha, scaleX , scaleY);
        animatorSet.setDuration(300);
        animatorSet.start();

    }

    public static void rotateAnimate(View view, boolean reverse) {
        float start = reverse ? 0f : -45f;
        float end = reverse ? 45f : 0f;
        ObjectAnimator animator = ObjectAnimator.ofFloat(view, "rotation", start, end);
        animator.setDuration(200);
        animator.start();
    }
}
