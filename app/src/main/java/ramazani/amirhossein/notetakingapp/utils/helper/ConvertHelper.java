package ramazani.amirhossein.notetakingapp.utils.helper;

import android.graphics.Typeface;

/**
 * Created by amirhossein on 12/12/2016.
 */

public class ConvertHelper {

    public static final String TEXT_NORMAL = "normal";
    public static final String TEXT_BOLD = "bold";
    public static final String TEXT_ITALIC = "italic";
    public static final String TEXT_BOLD_ITALIC = "bold_italic";

    public static int convertStringToStyle(String text) {

        if (text == null || text.length() == 0)
            return -1;

        if (text.equals(TEXT_NORMAL))
            return Typeface.NORMAL;
        else if (text.equals(TEXT_BOLD))
            return Typeface.BOLD;
        else if (text.equals(TEXT_ITALIC))
            return Typeface.ITALIC;
        else if (text.equals(TEXT_BOLD_ITALIC))
            return Typeface.BOLD_ITALIC;
        else
            return -1;
    }

    public static String convertStyleToString(int style) {
        switch (style) {
            case Typeface.NORMAL:
                return TEXT_NORMAL;
            case Typeface.BOLD:
                return TEXT_BOLD;
            case Typeface.ITALIC:
                return TEXT_ITALIC;
            case Typeface.BOLD_ITALIC:
                return TEXT_BOLD_ITALIC;
            default:
                return TEXT_NORMAL;
        }
    }

    public static boolean convertIntToBoolean(int boolInt) {
        return boolInt == 1 ? true : false;
    }

    public static int convertBooleanToInt(boolean bool) {
        return bool ? 1 : 0;
    }

    public static String convertColorToString(int color) {
        return String.format("#%06X", (0xFFFFFF & color));
    }

}
