package ramazani.amirhossein.notetakingapp.utils.helper;

import android.text.format.DateFormat;

import java.util.Calendar;

/**
 * Created by amirhossein on 12/12/2016.
 */

public class DateHelper {

    public static final String TYPE_SIMPLE = "yyyy-MM-dd hh:mm:ss a";
    public static final String TYPE_PAINT = "yyyyMMdd_HHmmss_SSS";
    public static final String TYPE_NAME = "yyy-MM-dd-HH-mm-ss";

    public static String formatNow(String... type) {
        return formatCalendar(Calendar.getInstance(), type);
    }

    public static String formatCalendar(Calendar calendar, String... type) {
        if (type == null || type.length == 0)
            return DateFormat.format(TYPE_SIMPLE, calendar.getTime()).toString();
        else
            return DateFormat.format(type[0], calendar.getTime()).toString();
    }
}
