package ramazani.amirhossein.notetakingapp.utils;

import android.os.Environment;

/**
 * Created by amirhossein on 12/6/2016.
 */

public class Constants {

    public static final String DATABASE_NAME = " Bazaar Notes";
    public static final String ARGUMENT_CAT_ID = "category_id";
    public static final String INTENT_CURRENT_NOTE = "current_note";
    public static final String INTENT_SELECTED_CATEGORY = "selected_category";
    public static final String EXTENSION_PAINT = ".png";

    public static final float ALPHA_FAB = 0.3F;
    public static final String INTENT_CURRENT_CATEGORY = "category";

    public static final String DIR_SDCARD = Environment.getExternalStorageDirectory().getAbsolutePath();
    public static final String DIR_APP = DIR_SDCARD + "/note-taking/";


}
