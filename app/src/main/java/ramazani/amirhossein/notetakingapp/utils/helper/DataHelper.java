package ramazani.amirhossein.notetakingapp.utils.helper;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import ramazani.amirhossein.notetakingapp.utils.Constants;

import static ramazani.amirhossein.notetakingapp.utils.Constants.DATABASE_NAME;

/**
 * Created by amirhossein on 12/16/2016.
 */

public class DataHelper {

    public static final String TAG = DataHelper.class.getSimpleName();

    public static File[] getBackupList() {
        File file = new File(Constants.DIR_APP);
        if (!file.exists()) {
            return null;
        } else {
            return file.listFiles();
        }

    }

    public static boolean importDB(Context context, File backupFile) {
        try {
            File data = Environment.getDataDirectory();

            if (backupFile.canRead()) {

                context.deleteDatabase(Constants.DATABASE_NAME);

                String currentDBPath = "//data//" + context.getPackageName()
                    + "//databases//" + DATABASE_NAME;

                File currentDB = new File(data, currentDBPath);

                FileChannel src = new FileInputStream(backupFile).getChannel();
                FileChannel dst = new FileOutputStream(currentDB).getChannel();
                dst.transferFrom(src, 0, src.size());

                src.close();
                dst.close();
                Toast.makeText(context, "Import Successful!",
                    Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Import Failed!", Toast.LENGTH_SHORT)
                .show();
            return false;
        }

        return true;
    }

    public static void exportDB(Context context, String name) {
        try {
            File sd = new File(Constants.DIR_APP);
            sd.mkdirs();

            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//" + context.getPackageName()
                    + "//databases//" + DATABASE_NAME;
                String backupDBPath = name;

                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();

                Toast.makeText(context, "Backup Successful!",
                    Toast.LENGTH_SHORT).show();

            }
        } catch (Exception e) {
            Log.e(TAG, "exportDB: ", e);
            Toast.makeText(context, "Backup Failed!", Toast.LENGTH_SHORT)
                .show();

        }
    }

}
