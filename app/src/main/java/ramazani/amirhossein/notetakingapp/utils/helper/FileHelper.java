package ramazani.amirhossein.notetakingapp.utils.helper;

import android.content.Context;
import android.util.Log;

import java.io.File;

import ramazani.amirhossein.notetakingapp.utils.Constants;

/**
 * Created by amirhossein on 12/15/2016.
 */

public class FileHelper {

    public static final String TAG = FileHelper.class.getSimpleName();

    public static File createPaintFile(Context context) {
        String fileName = DateHelper.formatNow(DateHelper.TYPE_PAINT);
        fileName += Constants.EXTENSION_PAINT;
        File file = new File(context.getExternalFilesDir(null), fileName);
        Log.i(TAG, "createPaintFile: " +  file.getAbsolutePath());
        return file;
    }

}
