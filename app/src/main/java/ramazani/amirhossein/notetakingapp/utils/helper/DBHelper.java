package ramazani.amirhossein.notetakingapp.utils.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import ramazani.amirhossein.notetakingapp.utils.Constants;

/**
 * Created by amirhossein on 12/6/2016.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static final String TAG = DBHelper.class.getSimpleName();

    private static DBHelper sDBHelper;
    private static SQLiteDatabase sDatabase;

    //DATABASE
    private static final String DATABASE_NAME = Constants.DATABASE_NAME;
    private static final int DATABASE_VERSION = 6;

    //TABLE NOTES
    public static final String TABLE_NOTES = "notes";

    public static final String COLUMN_NOTE_ID = "_id";
    public static final String COLUMN_NOTE_IS_TEXT = "is_text";
    public static final String COLUMN_NOTE_URI = "uri";
    public static final String COLUMN_NOTE_TITLE = "title";
    public static final String COLUMN_NOTE_CONTENT = "content";
    public static final String COLUMN_NOTE_STYLE = "text_style";
    public static final String COLUMN_NOTE_CATEGORY_ID = "category_id";
    public static final String COLUMN_NOTE_LAST_MODIFICATION = "last_modification";

    // TABLE CATEGORIES
    public static final String TABLE_CATEGORIES = "categories";

    public static final String COLUMN_CATEGORY_ID = "_id";
    public static final String COLUMN_CATEGORY_NAME = "name";
    public static final String COLUMN_CATEGORY_COLOR = "color";

    //CREATION
    private static final String CREATE_NOTES =
        "CREATE TABLE " + TABLE_NOTES
            + " ( "
            + COLUMN_NOTE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_NOTE_IS_TEXT + " INTEGER, "
            + COLUMN_NOTE_URI + " TEXT, "
            + COLUMN_NOTE_TITLE + " TEXT, "
            + COLUMN_NOTE_CONTENT + " TEXT, "
            + COLUMN_NOTE_STYLE + " TEXT, "
            + COLUMN_NOTE_CATEGORY_ID + " INTEGER DEFAULT NULL, "
            + COLUMN_NOTE_LAST_MODIFICATION + " INTEGER "
            + ");";

    private static final String CREATE_CATEGORIES =
        "CREATE TABLE " + TABLE_CATEGORIES
            + " ( "
            + COLUMN_CATEGORY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_CATEGORY_NAME + " TEXT, "
            + COLUMN_CATEGORY_COLOR + " TEXT"
            + " );";


    public static synchronized SQLiteDatabase getDataBaseInstance(Context context) {
        if (sDBHelper == null) {
            sDBHelper = new DBHelper(context);
        }

        if (sDatabase == null) {
            sDatabase = sDBHelper.getWritableDatabase();
        }

        return sDatabase;

    }

    public static SQLiteDatabase init(Context context) {
        sDBHelper = new DBHelper(context);
        sDatabase = sDBHelper.getWritableDatabase();
        return sDatabase;
    }

    private DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_NOTES);
        sqLiteDatabase.execSQL(CREATE_CATEGORIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTES);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORIES);

        onCreate(sqLiteDatabase);
    }
}
