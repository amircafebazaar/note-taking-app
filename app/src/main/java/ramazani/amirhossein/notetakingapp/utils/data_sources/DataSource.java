package ramazani.amirhossein.notetakingapp.utils.data_sources;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ramazani.amirhossein.notetakingapp.model.ModelCategory;
import ramazani.amirhossein.notetakingapp.model.ModelNote;
import ramazani.amirhossein.notetakingapp.utils.helper.ConvertHelper;
import ramazani.amirhossein.notetakingapp.utils.helper.DBHelper;

/**
 * Created by amirhossein on 12/7/2016.
 */

public class DataSource {

    public static final String TAG = DataSource.class.getSimpleName();

    private SQLiteDatabase database;

    private static final String[] QUERY_COLUMNS_NOTE = {
        DBHelper.COLUMN_NOTE_ID,
        DBHelper.COLUMN_NOTE_IS_TEXT,
        DBHelper.COLUMN_NOTE_URI,
        DBHelper.COLUMN_NOTE_TITLE,
        DBHelper.COLUMN_NOTE_CONTENT,
        DBHelper.COLUMN_NOTE_STYLE,
        DBHelper.COLUMN_NOTE_CATEGORY_ID,
        DBHelper.COLUMN_NOTE_LAST_MODIFICATION
    };


    private static final String[] QUERY_COLUMNS_CATEGORY = {
        DBHelper.COLUMN_CATEGORY_ID,
        DBHelper.COLUMN_CATEGORY_NAME,
        DBHelper.COLUMN_CATEGORY_COLOR
    };

    public DataSource(Context context) {
        database = DBHelper.getDataBaseInstance(context);
    }

    public void reInit(Context context) {
        database= DBHelper.init(context);
    }

    public synchronized List<ModelNote> getNotes(int catId) {

        if (catId == 0) {
            return getNotes();
        }

        List<ModelNote> notes = new ArrayList<>();
        String query = "SELECT * FROM " + DBHelper.TABLE_NOTES + " WHERE " + DBHelper.COLUMN_NOTE_CATEGORY_ID + "=" + catId;
        Cursor result = database.rawQuery(query, null);
        if (result.moveToFirst()) {
            do {
                ModelNote note = cursorToNote(result);
                note.setCategory(getCategory(catId));
                notes.add(note);
            }
            while (result.moveToNext());
        }

        result.close();

        return notes;
    }

    public synchronized List<ModelNote> getNotes() {
        List<ModelNote> notes = new ArrayList<>();
        Cursor query = database.query(DBHelper.TABLE_NOTES, QUERY_COLUMNS_NOTE, null, null, null, null, null);
        if (query.moveToFirst()) {
            do {
                ModelNote note = cursorToNote(query);
                note.setCategory(getNoteCategory(note.getNoteId()));
                notes.add(note);
            }
            while (query.moveToNext());
        }

        query.close();

        return notes;
    }

    public synchronized List<ModelCategory> getCategories() {
        List<ModelCategory> categories = new ArrayList<>();
        Cursor query = database.query(DBHelper.TABLE_CATEGORIES, QUERY_COLUMNS_CATEGORY, null, null, null, null, null);
        if (query.moveToFirst()) {
            do {
                ModelCategory category = cursorToCategory(query);
                String stringCountQuery = "SELECT COUNT(*) FROM " + DBHelper.TABLE_NOTES + " WHERE " + DBHelper.COLUMN_NOTE_CATEGORY_ID + "=" + category.getCategoryId();
                Cursor countQuery = database.rawQuery(stringCountQuery, null);
                if (countQuery.moveToFirst()) {
                    category.setNotesCount(countQuery.getInt(countQuery.getColumnIndex("COUNT(*)")));
                    categories.add(category);
                    countQuery.close();
                }
            } while (query.moveToNext());
        }

        query.close();
        return categories;
    }

    public synchronized ModelCategory getCategory(int catId) {
        String query = "SELECT * FROM " + DBHelper.TABLE_CATEGORIES + " WHERE " + DBHelper.COLUMN_CATEGORY_ID + "=" + catId;
        Cursor result = database.rawQuery(query, null);
        ModelCategory category = null;
        if (result.moveToFirst())
            category = cursorToCategory(result);

        result.close();

        return category;
    }

    public synchronized ModelCategory getNoteCategory(int noteId) {
        String query = "SELECT " + DBHelper.COLUMN_NOTE_CATEGORY_ID + " FROM " + DBHelper.TABLE_NOTES + " WHERE " + DBHelper.COLUMN_NOTE_ID + "=" + noteId;
        Cursor result = database.rawQuery(query, null);
        ModelCategory category = null;
        if (result.moveToFirst()) {
            int catId = result.getInt(result.getColumnIndex(DBHelper.COLUMN_NOTE_CATEGORY_ID));
            return getCategory(catId);
        }

        result.close();
        return category;
    }


    public synchronized void createNote(ModelNote note) {
        ContentValues values = createContentValueFromNote(note);
        database.insert(DBHelper.TABLE_NOTES, null, values);
    }

    public synchronized void updateNote(ModelNote note) {
        ContentValues values = createContentValueFromNote(note);
        database.update(DBHelper.TABLE_NOTES, values, DBHelper.COLUMN_NOTE_ID + "=" + note.getNoteId(), null);
    }

    public synchronized void removeNotes(List<ModelNote> notes) {
        for (ModelNote note : notes) {
            removeNote(note.getNoteId());
        }
    }

    public synchronized void removeNote(int id) {
        database.delete(DBHelper.TABLE_NOTES, DBHelper.COLUMN_NOTE_ID + "=" + id, null);
    }

    public synchronized void updateCategory(ModelCategory category) {
        ContentValues values = createContentValueFromCategory(category);
        database.update(DBHelper.TABLE_CATEGORIES, values, DBHelper.COLUMN_CATEGORY_ID + "=" + category.getCategoryId(), null);
    }

    public synchronized void createCategory(ModelCategory category) {
        ContentValues values = createContentValueFromCategory(category);
        database.insert(DBHelper.TABLE_CATEGORIES, null, values);
    }

    public synchronized void removeCategories(List<ModelCategory> categories) {
        for (ModelCategory category : categories) {
            removeCategory(category);
        }
    }

    public synchronized void removeCategory(ModelCategory category) {
        database.delete(DBHelper.TABLE_CATEGORIES, DBHelper.COLUMN_CATEGORY_ID + "=" + category.getCategoryId(), null);
        List<ModelNote> notes = getNotes(category.getCategoryId());
        for (ModelNote note : notes) {
            note.setCategory(null);
            updateNote(note);
        }
    }

    private ContentValues createContentValueFromNote(ModelNote note) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_NOTE_IS_TEXT, ConvertHelper.convertBooleanToInt(note.isText()));

        if (note.getUri() != null)
            values.put(DBHelper.COLUMN_NOTE_URI, note.getUri().toString());

        values.put(DBHelper.COLUMN_NOTE_TITLE, note.getTitle());
        values.put(DBHelper.COLUMN_NOTE_CONTENT, note.getContent());
        values.put(DBHelper.COLUMN_NOTE_STYLE, ConvertHelper.convertStyleToString(note.getStyle()));
        values.put(DBHelper.COLUMN_NOTE_CATEGORY_ID, note.getCategory() == null ? null : note.getCategory().getCategoryId());
        values.put(DBHelper.COLUMN_NOTE_LAST_MODIFICATION, Calendar.getInstance().getTimeInMillis());
        return values;
    }

    private ContentValues createContentValueFromCategory(ModelCategory category) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_CATEGORY_NAME, category.getName());
        values.put(DBHelper.COLUMN_CATEGORY_COLOR, category.getColor());
        return values;
    }

    private ModelNote cursorToNote(Cursor cursor) {
        ModelNote note = new ModelNote()
            .setNoteId(cursor.getInt(cursor.getColumnIndex(DBHelper.COLUMN_NOTE_ID)))
            .setIsText(ConvertHelper.convertIntToBoolean(cursor.getInt(cursor.getColumnIndex(DBHelper.COLUMN_NOTE_IS_TEXT))))
            .setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_NOTE_TITLE)))
            .setStyle(ConvertHelper.convertStringToStyle(cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_NOTE_STYLE))))
            .setContent(cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_NOTE_CONTENT)))
            .setLastModification(cursor.getLong(cursor.getColumnIndex(DBHelper.COLUMN_NOTE_LAST_MODIFICATION)));

        if (!note.isText()) {
            String str;
            if ((str = cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_NOTE_URI))) != null)
                note.setUri(Uri.parse(str));
        }

        return note;
    }

    private ModelCategory cursorToCategory(Cursor cursor) {
        return new ModelCategory()
            .setCategoryId(cursor.getInt(cursor.getColumnIndex(DBHelper.COLUMN_CATEGORY_ID)))
            .setName(cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_CATEGORY_NAME)))
            .setColor(cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_CATEGORY_COLOR)));
    }
}
