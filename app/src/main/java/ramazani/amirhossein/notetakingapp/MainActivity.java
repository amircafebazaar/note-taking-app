package ramazani.amirhossein.notetakingapp;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.List;

import ramazani.amirhossein.notetakingapp.model.ModelCategory;
import ramazani.amirhossein.notetakingapp.ui.fragments.FragmentNotes;
import ramazani.amirhossein.notetakingapp.ui.fragments.FragmentSettings;
import ramazani.amirhossein.notetakingapp.utils.Constants;
import ramazani.amirhossein.notetakingapp.utils.data_sources.DataSource;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int NAV_ID_CATEGORY_FACTOR = -1;

    private List<ModelCategory> mCategories;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private Toolbar mToolbar;
    public NavigationView mNavigationView;

    private int selectedId;

    public DataSource mDataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDataSource = new DataSource(this);
        setContentView(R.layout.activity_main);
        initComponents();

    }

    private void initComponents() {

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close);
        mDrawerLayout.addDrawerListener(mDrawerToggle);

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        loadNavigationMenuItems();
    }

    private void setMenuCounter(int count, @IdRes int id) {
        TextView view = (TextView) mNavigationView.getMenu().findItem(id).getActionView();
        view.setText(count > 0 ? String.valueOf(count) : null);
    }

    private void setMenuCounter(int count, MenuItem menuItem) {
        TextView view = (TextView) menuItem.getActionView();
        view.setText(count > 0 ? String.valueOf(count) : null);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private MenuItem mPreviousMenuItem;

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        selectedId = item.getItemId();

        Bundle args = new Bundle();
        Class fragmentClass = null;

        switch (item.getItemId()) {
            case R.id.nav_notes:
                fragmentClass = FragmentNotes.class;
                break;
            case R.id.nav_settings:
                fragmentClass = FragmentSettings.class;
                break;
            default:
                fragmentClass = FragmentNotes.class;
                args.putInt(Constants.ARGUMENT_CAT_ID, item.getItemId() / NAV_ID_CATEGORY_FACTOR);
                break;
        }

        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
            fragment.setArguments(args);
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment).commit();

        item.setCheckable(true);
        item.setChecked(true);
        if (mPreviousMenuItem != null && mPreviousMenuItem != item) {
            mPreviousMenuItem.setChecked(false);
        }
        mPreviousMenuItem = item;
        setTitle(item.getTitle());
        mDrawerLayout.closeDrawers();

        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadNavigationMenuItems();
    }

    public void loadNavigationMenuItems() {
        AsyncGetMenuItems items = new AsyncGetMenuItems();
        items.execute();
    }

    private class AsyncGetMenuItems extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            List<ModelCategory> categories = mDataSource.getCategories();
            mCategories = categories;
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mNavigationView.getMenu().clear();
            mNavigationView.inflateMenu(R.menu.drawer_menu);
            Menu menu = mNavigationView.getMenu().getItem(0).getSubMenu();
            for (ModelCategory category : mCategories) {
                MenuItem item = menu.add(0, NAV_ID_CATEGORY_FACTOR * category.getCategoryId(), 1, category.getName());
                item.setActionView(R.layout.menu_counter);
                item.setIcon(R.drawable.ic_label);
                item.setCheckable(true);
                Drawable newIcon = (Drawable) item.getIcon();
                newIcon.mutate().setColorFilter(Color.parseColor(category.getColor()), PorterDuff.Mode.SRC_IN);
                item.setIcon(newIcon);
                setMenuCounter(category.getNotesCount(), item);
            }
            if (selectedId == 0) {
                mNavigationView.getMenu().performIdentifierAction(R.id.nav_notes, 0);
            } else {
                mNavigationView.getMenu().performIdentifierAction(selectedId, 0);
            }
        }
    }
}
