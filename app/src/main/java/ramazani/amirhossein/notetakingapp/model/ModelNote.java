package ramazani.amirhossein.notetakingapp.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by amirhossein on 12/6/2016.
 */

public class ModelNote implements Parcelable {

    private int noteId;
    private boolean isText;
    private Uri uri;
    private String title;
    private String content;
    private int style;
    private long lastModification;
    private ModelCategory category;

    public ModelNote() {
    }

    public ModelNote(boolean isText, Uri uri, String title, String content, int catId) {
        this.isText = isText;
        this.uri = uri;
        this.title = title;
        this.content = content;
        this.category = new ModelCategory().setCategoryId(catId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.noteId);
        dest.writeByte(this.isText ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.uri, flags);
        dest.writeString(this.title);
        dest.writeString(this.content);
        dest.writeInt(this.style);
        dest.writeLong(this.lastModification);
        dest.writeParcelable(this.category, flags);
    }

    protected ModelNote(Parcel in) {
        this.noteId = in.readInt();
        this.isText = in.readByte() != 0;
        this.uri = in.readParcelable(Uri.class.getClassLoader());
        this.title = in.readString();
        this.content = in.readString();
        this.style = in.readInt();
        this.lastModification = in.readLong();
        this.category = in.readParcelable(ModelCategory.class.getClassLoader());
    }

    public static final Creator<ModelNote> CREATOR = new Creator<ModelNote>() {
        @Override
        public ModelNote createFromParcel(Parcel source) {
            return new ModelNote(source);
        }

        @Override
        public ModelNote[] newArray(int size) {
            return new ModelNote[size];
        }
    };

    public boolean isText() {
        return isText;
    }

    public ModelNote setIsText(boolean isText) {
        this.isText = isText;
        return this;
    }

    public Uri getUri() {
        return uri;
    }

    public ModelNote setUri(Uri uri) {
        this.uri = uri;
        return this;
    }

    public int getNoteId() {
        return noteId;
    }

    public ModelNote setNoteId(int noteId) {
        this.noteId = noteId;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public ModelNote setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getContent() {
        return content;
    }

    public ModelNote setContent(String content) {
        this.content = content;
        return this;
    }

    public int getStyle() {
        return style;
    }

    public ModelNote setStyle(int style) {
        this.style = style;
        return this;
    }

    public long getLastModification() {
        return lastModification;
    }

    public ModelNote setLastModification(long lastModification) {
        this.lastModification = lastModification;
        return this;
    }

    public ModelCategory getCategory() {
        return category;
    }

    public ModelNote setCategory(ModelCategory category) {
        this.category = category;
        return this;
    }
}