package ramazani.amirhossein.notetakingapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import static android.R.attr.description;

/**
 * Created by amirhossein on 12/6/2016.
 */

public class ModelCategory implements Parcelable {

    private int categoryId;
    private String name;
    private String color;
    private int notesCount;

    public ModelCategory() {
    }

    public ModelCategory(String name, String color) {
        this.name = name;
        this.color = color;
    }

    protected ModelCategory(Parcel in) {
        categoryId = in.readInt();
        name = in.readString();
        color = in.readString();
        notesCount = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(categoryId);
        dest.writeString(name);
        dest.writeString(color);
        dest.writeInt(notesCount);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelCategory> CREATOR = new Creator<ModelCategory>() {
        @Override
        public ModelCategory createFromParcel(Parcel in) {
            return new ModelCategory(in);
        }

        @Override
        public ModelCategory[] newArray(int size) {
            return new ModelCategory[size];
        }
    };

    public int getCategoryId() {
        return categoryId;
    }

    public ModelCategory setCategoryId(int categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public String getName() {
        return name;
    }

    public ModelCategory setName(String name) {
        this.name = name;
        return this;
    }

    public String getColor() {
        return color;
    }

    public ModelCategory setColor(String color) {
        this.color = color;
        return this;
    }

    public int getNotesCount() {
        return notesCount;
    }

    public ModelCategory setNotesCount(int notesCount) {
        this.notesCount = notesCount;
        return this;
    }

    @Override
    public String toString() {
        return "ModelCategory{" +
            "color='" + color + '\'' +
            ", description='" + description + '\'' +
            ", name='" + name + '\'' +
            ", categoryId=" + categoryId +
            '}';
    }
}
