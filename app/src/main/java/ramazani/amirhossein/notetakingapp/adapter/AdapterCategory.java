package ramazani.amirhossein.notetakingapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ramazani.amirhossein.notetakingapp.R;
import ramazani.amirhossein.notetakingapp.model.ModelCategory;

/**
 * Created by amirhossein on 12/9/2016.
 */

public class AdapterCategory extends SelectableAdapter<AdapterCategory.CategoryViewHolder> {

    public static final String TAG = AdapterCategory.class.getSimpleName();

    private List<ModelCategory> mCategories;
    private Context mContext;

    private CategoryViewHolder.ClickListener mClickListener;

    public AdapterCategory(Context context, List<ModelCategory> categories, CategoryViewHolder.ClickListener clickListener) {
        mContext = context;
        mCategories = categories;
        this.mClickListener = clickListener;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_category, parent, false);
        return new CategoryViewHolder(view, mClickListener);
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        if (isSelected(position)) {
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.cardViewBackgroundSelected));
        } else {
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.cardViewBackgroundNotSelected));
        }

        holder.update(mCategories.get(position));
    }

    @Override
    public int getItemCount() {
        return mCategories.size();
    }

    public void removeItem(int position) {
        mCategories.remove(position);
        notifyItemRemoved(position);
    }

    private void removeRange(int positionStart, int itemCount) {
        for (int i = 0; i < itemCount; ++i) {
            mCategories.remove(positionStart);
        }
        notifyItemRangeRemoved(positionStart, itemCount);
    }

    public void removeItems(List<Integer> positions) {
        Collections.sort(positions, new Comparator<Integer>() {
            @Override
            public int compare(Integer lhs, Integer rhs) {
                return rhs - lhs;
            }
        });
        while (!positions.isEmpty()) {
            if (positions.size() == 1) {
                removeItem(positions.get(0));
                positions.remove(0);
            } else {
                int count = 1;
                while (positions.size() > count && positions.get(count).equals(positions.get(count - 1) - 1)) {
                    ++count;
                }

                if (count == 1) {
                    removeItem(positions.get(0));
                } else {
                    removeRange(positions.get(count - 1), count);
                }

                for (int i = 0; i < count; ++i) {
                    positions.remove(0);
                }
            }
        }
    }


    public static class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private CardView cardView;

        private AppCompatTextView txtCategoryTitle;
        private AppCompatImageView imgCategoryColor;

        private ClickListener mListener;

        public CategoryViewHolder(View itemView, ClickListener listener) {
            super(itemView);

            cardView = (CardView) itemView.findViewById(R.id.cardCategory);
            txtCategoryTitle = (AppCompatTextView) itemView.findViewById(R.id.txtCategoryTitle);
            imgCategoryColor = (AppCompatImageView) itemView.findViewById(R.id.imgCategoryColor);

            this.mListener = listener;

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

        }

        public void update(ModelCategory category) {
            txtCategoryTitle.setText(category.getName());
            imgCategoryColor.getDrawable().mutate().setColorFilter(Color.parseColor(category.getColor()), PorterDuff.Mode.SRC_ATOP);
        }

        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClicked(getLayoutPosition());
            }
        }

        @Override
        public boolean onLongClick(View view) {
            if (mListener != null) {
                return mListener.onItemLongClicked(getLayoutPosition());
            }

            return false;
        }

        public interface ClickListener {

            void onItemClicked(int position);

            boolean onItemLongClicked(int position);
        }

    }

}
