package ramazani.amirhossein.notetakingapp.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ramazani.amirhossein.notetakingapp.R;
import ramazani.amirhossein.notetakingapp.listener.ClickListener;
import ramazani.amirhossein.notetakingapp.model.ModelNote;
import ramazani.amirhossein.notetakingapp.view_holder.MyCustomViewHolder;

/**
 * Created by amirhossein on 12/9/2016.
 */

public class AdapterNote extends SelectableAdapter<MyCustomViewHolder> {

    public static final String TAG = AdapterNote.class.getSimpleName();

    public static final int ITEM_TEXT = 0;
    public static final int ITEM_IMAGE = 1;

    private List<ModelNote> mNotes;
    private Context mContext;

    private ClickListener mClickListener;

    public AdapterNote(Context context, List<ModelNote> notes, ClickListener clickListener) {
        mContext = context;
        mNotes = notes;
        this.mClickListener = clickListener;
    }

    @Override
    public MyCustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_TEXT) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_note_text, parent, false);
            return new NoteViewHolder(view, mClickListener, mContext);
        } else if (viewType == ITEM_IMAGE) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_note_paint, parent, false);
            return new PaintViewHolder(view, mClickListener, mContext);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(MyCustomViewHolder holder, int position) {
        if (isSelected(position)) {
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.cardViewBackgroundSelected));
        } else {
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.cardViewBackgroundNotSelected));
        }
        holder.update(mNotes.get(position));
    }

    @Override
    public int getItemCount() {
        return mNotes.size();
    }

    public void removeItem(int position) {
        mNotes.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (mNotes.get(position).isText()) {
            Log.i(TAG, "pos: " + position + " is text");
            return ITEM_TEXT;
        } else {
            Log.i(TAG, "pos: " + position + " is paint");
            return ITEM_IMAGE;
        }
    }

    private void removeRange(int positionStart, int itemCount) {
        for (int i = 0; i < itemCount; ++i) {
            mNotes.remove(positionStart);
        }
        notifyItemRangeRemoved(positionStart, itemCount);
    }

    public void removeItems(List<Integer> positions) {
        Collections.sort(positions, new Comparator<Integer>() {
            @Override
            public int compare(Integer lhs, Integer rhs) {
                return rhs - lhs;
            }
        });
        while (!positions.isEmpty()) {
            if (positions.size() == 1) {
                removeItem(positions.get(0));
                positions.remove(0);
            } else {
                int count = 1;
                while (positions.size() > count && positions.get(count).equals(positions.get(count - 1) - 1)) {
                    ++count;
                }

                if (count == 1) {
                    removeItem(positions.get(0));
                } else {
                    removeRange(positions.get(count - 1), count);
                }

                for (int i = 0; i < count; ++i) {
                    positions.remove(0);
                }
            }
        }
    }


    public static class NoteViewHolder extends MyCustomViewHolder {

        private AppCompatTextView txtNoteTitle;
        private AppCompatTextView txtNoteContent;

        public NoteViewHolder(View itemView, ClickListener listener, Context context) {
            super(itemView, listener, context);
            txtNoteTitle = (AppCompatTextView) itemView.findViewById(R.id.txtNoteTitle);
            txtNoteContent = (AppCompatTextView) itemView.findViewById(R.id.txtNoteContent);
        }

        public void update(ModelNote note) {
            txtNoteTitle.setText(note.getTitle());
            txtNoteContent.setText(note.getContent());
        }
    }

    public static class PaintViewHolder extends MyCustomViewHolder {

        private AppCompatTextView txtNoteTitle;
        private AppCompatImageView imgNotePaint;

        public PaintViewHolder(View itemView, ClickListener listener, Context context) {
            super(itemView, listener, context);
            txtNoteTitle = (AppCompatTextView) itemView.findViewById(R.id.txtNoteTitle);
            imgNotePaint = (AppCompatImageView) itemView.findViewById(R.id.imgNotePaint);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void update(final ModelNote note) {
            txtNoteTitle.setText(note.getTitle());
            cardView.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        int height = cardView.getHeight();
                        Log.i(TAG, "card view height : " + height);
                        Uri imageUri = note.getUri();
                        Bitmap bitmap = null;
                        bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), imageUri);
                        Log.i(TAG, "bitmap height : " + bitmap.getHeight());
                        Log.i(TAG, "bitmap weight : " + bitmap.getWidth());
                        float per = (float) bitmap.getWidth() / bitmap.getHeight();
                        Log.i(TAG, "card view width : " + height * per);
                        imgNotePaint.setLayoutParams(new LinearLayoutCompat.LayoutParams((int) (height * per), LinearLayoutCompat.LayoutParams.MATCH_PARENT));
                        imgNotePaint.setImageBitmap(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

}
